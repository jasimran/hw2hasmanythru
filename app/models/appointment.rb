class Appointment < ActiveRecord::Base
  validates :fee, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  belongs_to :specialist
  belongs_to :patient
end
