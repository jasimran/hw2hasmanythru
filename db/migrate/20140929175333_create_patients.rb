class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.string :street_address
      t.string :insurance_id

      t.timestamps
    end
  end
end
