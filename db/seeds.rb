# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Insurance.create(name: 'Gecko', street_address: '236 Longway Dr')
Insurance.create(name: 'Mega Insurance', street_address: '36 Dockhall Court')
Insurance.create(name: 'Big Insurance', street_address: '5669 Fremall')
Insurance.create(name: 'Farmers', street_address: '289 Detroit St')
Insurance.create(name: 'All Life', street_address: '12 Marchway St')

Patient.create(name: 'Johnny Smite', street_address: '988 Con Mills', insurance_id: 5)
Patient.create(name: 'Bob Smith', street_address: '85 Strain alley', insurance_id: 4)
Patient.create(name: 'Maddy Tom', street_address: '169 Fidge Park', insurance_id: 3)
Patient.create(name: 'Bill John', street_address: '5782 Max lane', insurance_id: 2)
Patient.create(name: 'Rob Cruss', street_address: '8362 Calcu street', insurance_id: 1)

Specialist.create(name: 'Stanley Park', specialty: 'Lungs')
Specialist.create(name: 'Wu Smith', specialty: 'Cardiac')
Specialist.create(name: 'Daniel Lang', specialty: 'Orthopedics')
Specialist.create(name: 'Bob Bill', specialty: 'Surgery')
Specialist.create(name: 'Max Mill', specialty: 'Eyes')


Appointment.create(specialist_id: 5, patient_id: 1,complaint:'Too slow',appointment_date: '2014-12-22')
Appointment.create(specialist_id: 4, patient_id: 1,complaint:'Too fast',appointment_date: '2014-11-15')
Appointment.create(specialist_id: 3, patient_id: 2,complaint:'',appointment_date: '2014-11-11')
Appointment.create(specialist_id: 2, patient_id: 2,complaint:'',appointment_date: '2014-12-06')
Appointment.create(specialist_id: 1, patient_id: 3,complaint:'',appointment_date: '2014-12-14')
Appointment.create(specialist_id: 5, patient_id: 3,complaint:'',appointment_date: '2014-10-27')
Appointment.create(specialist_id: 4, patient_id: 4,complaint:'Too dirty',appointment_date: '2014-12-05')
Appointment.create(specialist_id: 3, patient_id: 4,complaint:'Too slow',appointment_date: '2014-11-19')
Appointment.create(specialist_id: 2, patient_id: 5,complaint:'',appointment_date: '2014-12-08')
Appointment.create(specialist_id: 1, patient_id: 5,complaint:'Too fast',appointment_date: '2014-10-29')